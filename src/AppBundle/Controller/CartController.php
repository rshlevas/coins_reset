<?php

    // src/AppBundle/Controller/CartController.php
    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use AppBundle\Entity\Product;
    use AppBundle\Entity\User;
    use AppBundle\Entity\Order;
    use AppBundle\Form\OrderType;
    use AppBundle\Services\SortService;
    use Doctrine\ORM\EntityManagerInterface;
    use Doctrine\Common\Persistence\ManagerRegistry;
    
    class CartController extends Controller 
    {
        /**
         * @Route("/cart/", name="cart_view", requirements={"id": "\d+"})
         */
        public function viewAction(Session $session, EntityManagerInterface $em)
        {
            $productsInCart = $session->get('products');
            if ($productsInCart == NULL) {
                return $this->render('cart/view.html.twig', [
                'products' => NULL,
                ]);
            }
            $products = self::getProducts($em, $productsInCart);
            $summ = self::getSumm($session, $products, $productsInCart);
            return $this->render('cart/view.html.twig', [
                'products' => $products,
                'summ' => $summ,
                'count' => $productsInCart,
                ]);
        }
        
        /**
         * @Route("/cart/{id}/", requirements={"id": "\d+"})
         * @Method({"GET", "POST"})
         */
        public function addAction($id, Session $session)
        {
            $productsInCart = $session->get('products');
            if (isset($productsInCart[$id])) {
                $productsInCart[$id] ++;
            } else {
                $productsInCart[$id] = 1;
            }
            $session->set('products', $productsInCart);
            return $this->countAction($session);
        }
        
        /**
         * @Route("/cart/count", name="cart_count")
         */
        public function countAction(Session $session)
        {
            if ($session->get('products')) {
                $count = 0;
                foreach ($session->get('products') as $key => $value) {
                    $count = $count + $value;
                }
                return new Response("($count)");
            }   
            else {
                return new Response(0);
            }
        }
        
        /**
         * @Route("/cart/delete/{id}/", requirements={"id": "\d+"})
         * @Method({"GET", "POST"})
         */
        public function deleteAction($id, Session $session, EntityManagerInterface $em)
        {
            $productsInCart = $session->get('products');
            unset($productsInCart[$id]);
            $session->set('products', $productsInCart);
            if ($productsInCart == NULL) {
                $html = $this->renderView('cart/no_products.html.twig');
                return new \Symfony\Component\HttpFoundation\JsonResponse(['html' => $html]);
            }
            $products = self::getProducts($em, $productsInCart);
            $summ = self::getSumm($session, $products, $productsInCart);
            $html = $this->renderView('cart/products_table.html.twig', [
                'products' => $products,
                'summ' => $summ,
                'count' => $productsInCart,
                ]);
            return new \Symfony\Component\HttpFoundation\JsonResponse(["html" => $html]);
        }
        
        /**
         * @Route("/cart/checkout", name="cart_checkout")
         */
        public function checkoutAction(Session $session, EntityManagerInterface $em, Request $request)
        {
            $productsInCart = $session->get('products');
            if ($productsInCart == NULL) {
                return $this->redirectToRoute('cart_view');
            }
            $products = self::getProducts($em, $productsInCart);
            $product_info = SortService::formProductsInfoForOrder($products, $productsInCart);
            $summ = $session->get('summ');
            $user = $this->getUser();
            $options = [];
            if (is_object($user) || $user instanceof UserInterface) {
                $options['email'] = $user->getEmail();
            }
            $form = $this->createForm(OrderType::class, $options);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $user_info = SortService::formUserInfoForOrder($data, $user);
                $order = new Order();
                $order = self::prepareOrder($order, $product_info, $user_info, $data, $user, $summ);
                $em->persist($order);
                $em->flush();
                if ($order->getId()) {   
                    $session->clear('products');
                    $session->clear('summ');
                    //Send email to customer and to manager
                    //Send sms to customer
                    return $this->render('cart/checkout_success.html.twig');
                } else {
                    return $this->render('cart/checkout_mistake.html.twig');
                }
            }
            return $this->render('cart/checkout.html.twig', [
                'form' => $form->createView(),
            ]);
            
        }
        
       private static function getProducts(EntityManagerInterface $em, $productsInCart)
        {
            $products = [];
            $repository = $em->getRepository('AppBundle:Product');
            foreach ($productsInCart as $id => $count) {
                $products[] = $repository->findOneById($id);
            }
            return $products;
        }
        
        private static function getSumm(Session $session, $products, $productsInCart) 
        {
            $summ = 0;
            foreach ($products as $product) {
                $summ = $summ + $product->getPrice()*$productsInCart[$product->getId()];
            }
            $session->set('summ', $summ);
            return $summ;
        }
        
        private static function prepareOrder(
                Order $order, 
                $product_info, 
                $user_info, 
                $data, 
                $user, 
                $summ
            )
        {
            $order->setProducts(json_encode($product_info));
            $order->setUser(json_encode($user_info));
            $order->setDeliveryType($data['delivery_type']);
            $order->setPaymentType($data['payment_type']);
            $order->setDate(new \DateTime);
            $order->setStatus(0);
            $order->setSumm($summ);
            if (is_object($user) || $user instanceof UserInterface) {
               $order->setUserId($user->getId()); 
               $order->setUserInfo($user);
            }
            return $order;
        }
    }