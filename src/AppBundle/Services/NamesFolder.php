<?php

 // src/AppBundle/Services/NamesFolder.php
    namespace AppBundle\Services;

class NamesFolder
{
    private static function getTypeCollections()
    {
        $types = [
            0 => [
                'id' => 1,
                'name' => 'Монеты',
                'route' => 'monety'
            ],
            1 => [
                'id' => 2,
                'name' => 'Банкноты',
                'route' => 'banknoty'
            ],
            2 => [
                'id' => 3,
                'name' => 'Аксессуары',
                'route' => 'aksesuary'
            ],
        ];
        
        return $types;
    }
    
    /*
     * Static function to return type_id by string
     */
    public static function getType($type)
    {
        $types = self::getTypeCollections();
        foreach ($types as $type_item) {
            if ($type_item['route'] == $type) {
                return $type_item['id'];
            }
        }
        return -1;
    }

    /*
     * Static function to return type_route by id
     */
    public static function getTypeRouteById($type_id)
    {
        $types = self::getTypeCollections();
        foreach ($types as $type_item) {
            if ($type_item['id'] == $type_id) {
                return $type_item;
            }
        }
    }
    
    /*
     * Static function to return region_id by string
     */
    public static function getRegion($region)
    {
        $region_ids = [
            'europe' => 1,
            'africa' => 2,
            'n_america' => 3,
            's_america' => 4,
            'asia' => 5,
            'australia' => 6,
            'other' => 7
        ];
        foreach ($region_ids as $id) {
            if ($region_ids[$region]) {
                return $region_ids[$region];
            }
        }
        return -1;
    }
    
    /*
     * Static function to return region collection
     */
    public static function getRegionCollection()
    {
        $regions = [
            1 => 'europe',
            2 => 'africa',
            3 => 'n_america',
            4 => 's_america',
            5 => 'asia',
            6 => 'australia',
            7 => 'other'
        ];
        return $regions;
    }
    
    public static function getDeliveryType()
    {
        $delivery_type = [
            'Новая почта' => 0,
            'Укрпошта' => 1,
            'Из офиса' => 2,
        ];
        return $delivery_type;
    }
    
    public static function getPaymentType()
    {
        $payment_type = [
            'Почтовый перевод' => 0,
            'Перевод на карту Приват Банка' => 1,
            'Наличными' => 2,
        ];
        return $payment_type;
    }
    
    public static function getOrderStatus()
    {
        $order_status = [
            'Оформлен' => 0,
            'В обработке' => 1,
            'Оплачен' => 2,
            'Доставлен' => 3,
        ];
        return $order_status;
    }
    public static function getOrderStatusByNumber($number)
    {
        $order_status = self::getOrderStatus();
        foreach ($order_status as $key => $value) {
            if ($value == $number) {
                return $key;
            } else {
                return -1;
            }
        }
    }
    
}
   
